# Configuration of apache and MySQL 

This file serves to explain the script to the user.
<br>

<h2>Contents</h2>
<ul>
<li>Introduction</li>
<li>Requirements</li>
<li>Installation</li>
<li>Configuration</li>
<li>Maintainers</li>
</ul>

<h2>Introduction</h2>

The script serves the user to automate the configuration of apache and MySQL. 
Thanks to the automation the process will be done faster, so the work off the user will be reduced. 
 
<br>

<h2>Requirements</h2>

<ul>
<li>Virtual Machine</li>
<li>Ubuntu-Server</li>
</ul>

<br><br>

<h2>Installation</h2>
If you haven't installed a Virtual Machine yet, please do that as first step. 
Import the Ubuntu-Server into your Virtual Machine. 

<br><br>

<h2>Configuration</h2>
The steps are shown with an example.



<h2>Maintainers</h2>
<ul>
<li>Gioele Petrillo - <a href= "gioele.petrillo@bwz-rappi.ch">gioele.petrillo@bwz-rappi.ch</a></li>
<li>Daniela Lambaré - <a href= "daniela.lambare@bwz-rappi.ch">daniela.lambare@bwz-rappi.ch</a></li>
<li>Fatima Salem - <a href= "fatima.salembokretache@bwz-rappi.ch">fatima.salembokretache@bwz-rappi.ch</a></li>
</ul>
